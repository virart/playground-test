!function (w, d) {
    let throttter_timeout = 300,
        waiting = false,
        type_throttler,
        textarea = d.getElementById('markdown_text'),
        result = d.getElementById('demo-result')
    ;


    /**
     * Слушаем события ввода с клавиатуры.
     */
    function typeListener() {
        if(type_throttler) return;
        type_throttler = true;
        setTimeout(() => {
                sendRequest();
                type_throttler = false;
            },
            throttter_timeout
        )
    }

    function sendRequest() {
        fetch('/markdown/transform.json',{
            method:'POST',
            body:'{"text":'+JSON.stringify(textarea.value)+'}'
        })
            .then(response => {
                if (response.status != 200) {
                    return false;
                }
                return response.json();
            })
            .then(response => {
                if(!response) return;
                result.innerHTML = response.html;
            });
    }

    function init() {
        textarea.addEventListener('keypress', typeListener);
    }

    init();
}(window, document);
