<?php

namespace App\Tests\Service;

use App\Service\MarkdownToHtmlService;
use PHPUnit\Framework\TestCase;

class MarkdownToHtmlServiceTest extends TestCase
{
    /** @var MarkdownToHtmlService  */
    protected $service;
    protected $em_tag='em';
    protected $strong_tag='strong';


    public function setUp(): void
    {
        parent::setUp();
        $this->service = new MarkdownToHtmlService();
    }

    /**
     * Check converting Markdown italics (emphasis) and bold (strong) to HTML
     * @dataProvider italicAndBoldDataProvider
     */
    public function testTransformItalicsAndBold($text, $res)
    {
        $text = $this->service->transformItalicsAndBold($text);
        $this->assertEquals($text, $res);
    }

    public function italicAndBoldDataProvider()
    {
        $s = $this->strong_tag;
        $e = $this->em_tag;
         return [
             //1
             [
                 '***EM AND BOLD***',
                 "<$s><$e>EM AND BOLD</$e></$s>"
             ],
             //2
             [
                 '**it is *nested em* in middle**',
                 "<$s>it is <$e>nested em</$e> in middle</$s>"
             ],
             //3
             [
                 '_it is __nested bold__ in middle_',
                 "<$e>it is <$s>nested bold</$s> in middle</$e>"
             ],
             //4
             [
                 '__ it is mistake in md syntax__',
                 "__ it is mistake in md syntax__"
             ],
             //5
             [
                 '__it is mistake in md syntax but __all text is strong__',
                 "<$s>it is mistake in md syntax but __all text is strong</$s>"
                 //'__ it is mistake in md syntax and _three italic words___'
             ],
             //6
             [
                '___There is_ two nested em\'s in bold in start and _end of text___',
                "<$s><$e>There is</$e> two nested em's in bold in start and <$e>end of text</$e></$s>"
             ],
             //7
             [
                 '***there is** two nested bolds in em in start and **end of text***',
                 "<$e><$s>there is</$s> two nested bolds in em in start and <$s>end of text</$s></$e>"
             ],
             //8
             [
                 'Only letter "o" is a b**o**ld',
                 "Only letter \"o\" is a b<$s>o</$s>ld"
             ],
             //9
             [
                 'Letter "t" is a i_t_alic',
                 "Letter \"t\" is a i<$e>t</$e>alic"
             ],
             //10
             [
                 "Multiline **strong \n words**",
                 "Multiline <$s>strong \n words</$s>"
             ],
             //11
             [
                 "_Cross **em and_ bold**",
                 "_Cross <$s>em and_ bold</$s>"
             ],
             //12
             [
                 "__Cross *bold and__ em*",
                 "<$s>Cross *bold and</$s> em*"
             ],

         ];
    }
}