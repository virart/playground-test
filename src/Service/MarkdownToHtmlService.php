<?php

namespace App\Service;
use Michelf\Markdown;

class MarkdownToHtmlService extends Markdown
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Convert Markdown italics (emphasis) and bold (strong) to HTML
     * @param  string $text
     * @return string
     */
    public function transformItalicsAndBold($text) {

        $tmp = [
            'document_gamut' => $this->document_gamut,
            'block_gamut' => $this->block_gamut,
            'span_gamut' => $this->span_gamut
        ];

        $this->document_gamut = array(
            //"stripLinkDefinitions" => 20,
            "runBasicBlockGamut"   => 30,
        );

        $this->block_gamut = array(
        );

        $this->span_gamut = array(
            // Process character escapes, code spans, and inline HTML
            // in one shot.
            //"parseSpan"           => -30,
            "doItalicsAndBold"    =>  50,
        );

        asort($this->document_gamut);
        asort($this->block_gamut);
        asort($this->span_gamut);

        $res = $this->transform($text);

        foreach ($tmp as $key => $value) {
            $this->$key = $value;
        }

        return substr($res, 0, -1);
    }

    protected function formParagraphs($text, $wrap_in_p = true) {
        // Strip leading and trailing lines:
        $text = preg_replace('/\A\n+|\n+\z/', '', $text);

        $grafs = preg_split('/\n{2,}/', $text, -1, PREG_SPLIT_NO_EMPTY);

        // Wrap <p> tags and unhashify HTML blocks
        foreach ($grafs as $key => $value) {
            if (!preg_match('/^B\x1A[0-9]+B$/', $value)) {
                // Is a paragraph.
                $value = $this->runSpanGamut($value);
//                if ($wrap_in_p) {
//                    $value = preg_replace('/^([ ]*)/', "<p>", $value);
//                    $value .= "</p>";
//                }
                $grafs[$key] = $this->unhash($value);
            } else {
                // Is a block.
                // Modify elements of @grafs in-place...
                $graf = $value;
                $block = $this->html_hashes[$graf];
                $graf = $block;
                $grafs[$key] = $graf;
            }
        }

        return implode("\n\n", $grafs);
    }

}

