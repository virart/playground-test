<?php

namespace App\Controller;

use App\Form\MarkdownType;
use App\Service\MarkdownToHtmlService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/markdown", name="markdown-")
 */
class MarkdownController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(Request $request, MarkdownToHtmlService $markdown): Response
    {
        $form = $this->createForm(MarkdownType::class);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $text = $form->get('text')->getData();
            $html = $markdown->transformItalicsAndBold($text);

            return $this->render('markdown/result.html.twig', compact('text','html'));
        }

        return $this->render('markdown/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/transform.json", name="transform-json")
     */
    public function transform(Request $request, MarkdownToHtmlService $markdown): Response
    {
        $data = json_decode($request->getContent(), true);
        $text = $data['text'] ?? '';

        return new JsonResponse([
                'html' => $markdown->transformItalicsAndBold($text),
            ]);
    }
}
